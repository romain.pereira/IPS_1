/**
 * @file fact.cpp
 * @brief calcule la factorielle
 */

#include "fact.hpp"

/**
 *  Calcule la factorielle de n (n!)
 *  @param n l'entier
 *  @return n!
 */
unsigned int fact(unsigned int n) {
    static unsigned int r[13] = { 1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880,
                                  3628800, 39916800, 479001600
                                };
    return n < 13 ? r[n] : -1;
}

