/**
 *	\file main.cpp
 *	\brief	Contient le 'main' du programme
 *		et lis les arguments passés en paramètres
 */

# include <iostream>
# include <fstream>
# include "nlohmann/json.hpp"
# include "Oscillator.hpp"

/**
 * Charge le fichier de configuration passé en paramètre s'il existe,
 * ou charges une configuration par défaut sinon
 * @param argc nombre d'argument (taille de argv)
 * @param argv les arguments
 * @return un objet 'nlohmann::json' contenant la configuration pour la résolution
 */
nlohmann::json loadConfig(int argc, char **argv) {
    /* charge la config par défaut */
    nlohmann::json config;

    config["n"] = 3;

    config["z"] = nlohmann::json();
    config["z"]["min"] = -1.0;
    config["z"]["max"] = 1.0;
    config["z"]["points"] = 10;

    config["parameters"] = nlohmann::json();
    config["parameters"]["omega"] = 1.0;
    config["parameters"]["mass"] = 1.0;
    config["parameters"]["plank"] = 1.0;

    if (argc == 2) {
        try {
            std::ifstream config_if(argv[1]);
            config_if >> config;
        } catch (nlohmann::detail::parse_error & err) {
            std::cerr << "Le fichier " << argv[1] << " est invalide." << std::endl;
            std::cerr << err.what() << std::endl;
        }
    }
    return config;
}

/**
 * Main du programme
 *
 * Affiches successivement:
 * 	- la configuration de résolution (format JSON)
 * 	- le résultat sous forme matriciel
 * @param argc nombre d'argument (taille de argv)
 * @param argv les arguments
 * @return exit status
 */
int main(int argc, char **argv) {

    /* lecture du fichier de config, s'il est présent, sinon on initialise les valeurs par défaut */
    nlohmann::json config = loadConfig(argc, argv);
    std::cout << config << std::endl;

    /* création de l'oscillateur */
    double w = config["parameters"]["omega"];
    double m = config["parameters"]["mass"];
    double h = config["parameters"]["plank"];
    ips::Oscillator oscillator(w, m, h);

    /* création du vecteur des abscisses : discretisation uniforme de [z0, z1] */
    double z0 = config["z"]["min"];
    double z1 = config["z"]["max"];
    unsigned int points = config["z"]["points"];
    arma::vec z = arma::linspace<arma::vec>(z0, z1, points);

    /* calcul des solutions */
    unsigned int n = config["n"];
    arma::mat M = oscillator.solutions(z, n);
    M.raw_print();

    return 0;
}
