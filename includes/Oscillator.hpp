/**
 *	\file Oscillator.hpp
 *	\brief Interface de l'oscillateur harmonique
 */

#ifndef OSCILLATOR_HPP_
#define OSCILLATOR_HPP_

# include "nlohmann/json.hpp"
# include "fact.hpp"
# include "hermite.hpp"
# include <iostream>
# include <armadillo>

namespace ips {

class Oscillator {
  public:

    /**
     * Crée un nouveau oscillateur harmonique 1D
     * @param p_w fréquence omega
     * @param p_m masse m
     * @param p_h h 'barre' : constante de Plank
     */
    Oscillator(double p_w, double p_m, double p_h);
    virtual ~Oscillator();

    /**
     * Résout et affiches les solutions du problème
     * @param z le vecteur des absisses (coordonées d'espace 1D)
     * @param n l'indice de la solution dans la famille
     * @return la matrice des 'n' 1ères solutions, les lignes sont indexées par 'z', et les colonnes par 'n'
     */
    arma::mat solutions(arma::vec z, unsigned int n);

    /**
     * la fréquence oméga de l'oscillateur
     */
    double w;

    /**
     * la masse m de la particule
     */
    double m;

    /**
     * la constante de Plank a utilisé dans le calcul
     */
    double h;

};

} /* namespace ips */
#endif /* OSCILLATOR_HPP_ */

