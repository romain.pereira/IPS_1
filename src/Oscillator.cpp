/**
 *	\file Oscillator.cpp
 *	\brief Implementation de Oscillator.hpp
 */

# include "Oscillator.hpp"

namespace ips {

Oscillator::Oscillator(double p_w, double p_m, double p_h) :
    w(p_w), m(p_m), h(p_h) {
}

Oscillator::~Oscillator() {
}

arma::mat Oscillator::solutions(arma::vec z, unsigned int n) {

    /* récuperes la valeur des polynomes de Hermite */
    arma::mat M = hermite(sqrt(m * w / h) * z, n);

    /* multiplie chaque élément par m*w/(pi*hbar) */
    M *= sqrt(sqrt(m * w / (h * M_PI)));

    /* multiple chaque colonne par 1/sqrt(2^n*n!) */
    for (unsigned int i = 0 ; i <= n ; i++) {
        M.col(i) *= 1.0 / sqrt((1 << i) * fact(i));
    }

    /* multiple chaque ligne par exp(-m*w*z^2/(2*hbar)) */
    for (unsigned int i = 0 ; i < z.n_rows ; i++) {
        M.row(i) *= exp(-m * w / (2.0 * h) * z(i) * z(i));
    }

    return M;
}

} /* namespace ips */
