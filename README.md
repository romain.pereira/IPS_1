# IPS Projet 1

Ce projet a pour but de calculer les solutions d'un oscillateur harmonique quantique à une dimension. 

#### Récupérer le projet
```
git clone http://gitlab.pedago.ensiie.fr/romain.pereira/IPS_1.git
cd IPS_1
make
```

#### Exemple d'utilisation (calcul seulement)
```
./bin/main {config.json}
```
Un fichier 'config.json' d'exemple est disponible à la racine du projet

#### Exemple d'utilisation (calcul + affichage)
```
# affiches toutes les solutions
./bin/main {config.json} | python3.6 scripts/plot.py

# affiches les solutions pour n=1, n=3, n=4, n=9
./bin/main {config.json} | cut -d ' ' -f 1,3,4,9 | python3.6 scripts/plot.py
```

#### Horaire de passage
Mercredi 6 Novembre 2018 à 10h30
