import sys
import json 
import numpy as np

n = int(sys.argv[1]) if len(sys.argv) == 2 else 100
(x, w) = np.polynomial.hermite.hermgauss(n)
data = {}
data['x'] = x.tolist();
data['w'] = w.tolist();
json.dump(data, sys.stdout, sort_keys=True)
