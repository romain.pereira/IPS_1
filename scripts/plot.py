import json
import numpy as np
import matplotlib.pyplot as plt

"""
"	fonction qui affiche une courbe du resultat
"	@param f un descripteur de fichier
"""
def plot(f):
	# charge la config
	config = json.loads(f.readline())

	# génères les abscisses, et parse le résultat matriciel
	z = np.linspace(config["z"]["min"], config["z"]["max"], config["z"]["points"])
	M = np.loadtxt(f, dtype=np.float64)
	M = np.multiply(M, M)

	# affichage et légende des courbes
	plt.legend(plt.plot(z, M), ["i=%d" % i for i in range(config["n"] + 1)])

	# titre du graphe
	plt.title("Solution de la résolution d'un oscillateur harmonique\n(w, m, h) = (%f, %f, %f)" % (config["parameters"]["omega"], config["parameters"]["mass"], config["parameters"]["plank"]))

	# legende des axes
	plt.xlabel("position 'z'")
	plt.ylabel("probabilité de présence de la particule")

	# affiches le graphe
	plt.show()

# on lit sur l'entree standart
import sys
plot(sys.stdin)
