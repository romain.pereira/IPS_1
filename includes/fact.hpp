/**
 * @file fact.hpp
 * @brief calcule la factorielle
 */

#ifndef FACT_HPP
# define FACT_HPP


/**
 *  Calcule la factorielle de n (n!)
 *  @param n l'entier
 *  @return n!
 */
unsigned int fact(unsigned int n);

#endif
