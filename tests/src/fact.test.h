#include <cxxtest/TestSuite.h>
#include "fact.hpp"

class FactTestSuite: public CxxTest::TestSuite {
  public:
    void testFactorielle(void) {
        TS_ASSERT_EQUALS(fact(0), 1);
        TS_ASSERT_EQUALS(fact(1), 1);
        TS_ASSERT_EQUALS(fact(2), 2);
        TS_ASSERT_EQUALS(fact(3), 6);
        TS_ASSERT_EQUALS(fact(4), 24);
        TS_ASSERT_EQUALS(fact(12), 479001600);
    }

};
